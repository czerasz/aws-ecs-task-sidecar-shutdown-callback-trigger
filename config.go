package main

import (
	"fmt"
	"net/url"
	"strings"

	"github.com/kelseyhightower/envconfig"
	metadata "gitlab.com/czerasz/go-aws-ecs-metadata"
)

// Config represents the apps configuration
type Config struct {
	ContainerName string `split_words:"true" required:"true"`

	Interval    int64 `default:"10"`
	MaxDuration int64 `split_words:"true" default:"60"`

	MetadataEndpoint        string `envconfig:"AWS_ECS_TASK_METADATA_ENDPOINT"`
	MetadataEndpointVersion string `envconfig:"AWS_ECS_TASK_METADATA_ENDPOINT_VERSION"`

	ClbURL      string `envconfig:"CALLBACK_URL" required:"true"`
	ClbUser     string `envconfig:"CALLBACK_User"`
	ClbPassword string `envconfig:"CALLBACK_Password"`
}

// NewConfig creates new config with defaults as fallback
func NewConfig() (*Config, error) {
	var c Config
	err := envconfig.Process("", &c)

	if err != nil {
		return nil, err
	}

	if c.MetadataEndpointVersion == "" {
		c.MetadataEndpointVersion = metadata.AwsECSTaskMetadataEndpointVersion
	}

	return &c, nil
}

// CallbackURL returns callback URL and eventual error
func (c *Config) CallbackURL() (string, error) {
	u, err := url.Parse(c.ClbURL)
	if err != nil {
		return "", fmt.Errorf("could not parse callback URL: %w", err)
	}

	user, pass := strings.TrimSpace(c.ClbUser), strings.TrimSpace(c.ClbPassword)
	if user != "" && pass != "" {
		u.User = url.UserPassword(user, pass)
	}

	return u.String(), nil
}
