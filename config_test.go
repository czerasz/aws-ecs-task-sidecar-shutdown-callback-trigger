package main

import (
	"os"
	"testing"

	metadata "gitlab.com/czerasz/go-aws-ecs-metadata"
)

func TestNewConfig(t *testing.T) {
	var c *Config
	var err error

	os.Unsetenv("CALLBACK_URL")
	os.Unsetenv("CONTAINER_NAME")
	c, err = NewConfig()
	if err == nil {
		t.Errorf("when CONTAINER_NAME is unset the config should fail")
	}

	os.Setenv("CONTAINER_NAME", "pipeline")
	c, err = NewConfig()
	if err == nil {
		t.Errorf("when CALLBACK_URL is unset the config should fail")
	}

	os.Setenv("CALLBACK_URL", "http://localhost:8081/callback")
	c, err = NewConfig()
	if err != nil {
		t.Errorf("config should not fail")
	}
	if c.ContainerName != "pipeline" {
		t.Errorf("container name was not properly forwarded")
	}

	url, err := c.CallbackURL()
	if err != nil {
		t.Errorf("unexpected error: %s", err)
	}
	equals(t, url, "http://localhost:8081/callback")

	equals(t, c.MetadataEndpointVersion, metadata.AwsECSTaskMetadataEndpointVersion)
}
