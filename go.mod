module gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger

go 1.13

require (
	github.com/kelseyhightower/envconfig v1.4.0
	gitlab.com/czerasz/go-aws-ecs-metadata v0.2.0
)
