package main

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	metadata "gitlab.com/czerasz/go-aws-ecs-metadata"
)

func main() {
	c, err := NewConfig()
	if err != nil {
		log.Fatalf("could NOT initialize configuration: %s", err)
	}

	// Define starting time
	begin := time.Now()
	// define sleep duration
	sleep := time.Duration(c.Interval) * time.Second

	defaultTimeout := 5
	client := &http.Client{
		Timeout: time.Duration(defaultTimeout) * time.Second,
	}

	cbURL, err := c.CallbackURL()
	if err != nil {
		log.Fatalf("error when generating callback URL: %s", err)
	}

	for {
		meta, err := metadata.FetchV2Metadata(client, c.MetadataEndpoint)

		if err != nil {
			log.Fatalf("could not fetch metadata: %s", err)
		}

		stopped, exitCode, err := meta.IsContainerStopped(c.ContainerName)
		if err != nil {
			log.Fatalf("could not fetch metadata for container %s: %s", c.ContainerName, err)
		}

		if stopped {
			req, err := http.NewRequest(http.MethodGet, cbURL, nil)
			if err != nil {
				log.Fatalf("could not create callback request: %s", err)
			}

			// Add exit_code as query parameter
			q := req.URL.Query()
			q.Add("exit_code", strconv.Itoa(exitCode))
			req.URL.RawQuery = q.Encode()

			_, err = client.Do(req)
			if err != nil {
				log.Fatalf("callback request failed: %s", err)
			}

			// successfully exit process when required container was stopped
			os.Exit(0)
		}

		// exit process with failure when the time is over
		diff := time.Since(begin).Seconds()
		if int64(diff) > c.MaxDuration {
			log.Fatalf("maximum duration reached")
		}

		time.Sleep(sleep)
	}
}
