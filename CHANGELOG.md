## [0.3.1](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/compare/v0.3.0...v0.3.1) (2020-07-30)


### Code Refactoring

* outsource metadata logic ([6bf56da](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/6bf56dad9545bc71b1f9742a609107b0a271cb1b))

# [0.3.0](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/compare/v0.2.2...v0.3.0) (2020-07-29)


### Documentation

* add usage section ([b5cba8b](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/b5cba8b4aeae5ec6a91525979582c07ad4c635db))


### Features

* add support for CALLBACK_USER and CALLBACK_PASSWORD ([4d38b8d](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/4d38b8d7f40d11fa852f5891730f27dd7539c0e7))

## [0.2.2](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/compare/v0.2.1...v0.2.2) (2020-07-14)


### Bug Fixes

* missing callback exit_code parameter ([4fe5445](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/4fe5445d34637a0ace412b154eaad840686e684a))
* unable to initialize /etc/mtab ([ceb9008](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/ceb9008d29e828e2bcd5fc6a77f68a00d5a33ca8))

## [0.2.1](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/compare/v0.2.0...v0.2.1) (2020-07-14)


### Code Refactoring

* trigger CI ([2640f49](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/2640f49d753b9887e2465fcfa4d6a331f3f93702))


### Continuous Integrations

* do not skip the CI when creating the tag ([81b36ec](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/81b36ec02766cb1ae32440b08403debfce38ebc3))

# [0.2.0](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/compare/v0.1.0...v0.2.0) (2020-07-14)


### Code Refactoring

* use sleep instead of tick ([b0add99](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/b0add9931f55d29b3ea59449a41c413e7e6e4df5))


### Features

* append exit_code to the callback URL ([f30218f](https://gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger/commit/f30218fea55af497a5e3671175faba2f10162182))
