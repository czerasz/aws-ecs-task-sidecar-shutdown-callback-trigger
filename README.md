# AWS ECS Task Sidecar Shutdown Callback

Need to trigger a callback/hook after an AWS ECS task has finished?

If a HTTP service needs to be notified after the AWS ECS task has finished, then this repository and the related Docker image can help. Simply run a sidecar container next to the main container.

## Usage

Example container definitions:

```json
{
  "name": "pipeline",
  "essential": false,
  ...
},
{
  "name": "sidecar-stopped",
  "image": "registry.gitlab.com/czerasz/aws-ecs-task-sidecar-shutdown-callback-trigger:v0.2.2",
  "essential": true,

  "dependsOn": [
    {
      "containerName": "pipeline",
      "condition": "COMPLETE"
    }
  ],

  "environment": [
    {
      "name": "CONTAINER_NAME",
      "value": "pipeline"
    },
    {
      "name": "CALLBACK_URL",
      "value": "${callback_url}"
    }
  ],
  ...
}
```

## Overview

![Overview](docs/images/overview.png)

An ECS cluster runs the task. Inside the main container, assisted by a sidecar container.
The sidecar container will monitor the the main container via the AWS ECS task metadata endpoint.

## Environment Variables

| Environment Variable Name                | Type      | Required | Default                | Description |
| ---------------------------------------- | --------- | -------- | ---------------------- | ----------- |
| `CONTAINER_NAME`                         | `string`  | Yes      | -                      | The container which should be monitored |
| `CALLBACK_URL`                           | `string`  | Yes      | -                      | The URL which should be called after the observed container stops |
| `CALLBACK_USER`                          | `string`  | No       | -                      | Basic auth user for the callback URL |
| `CALLBACK_PASSWORD`                      | `string`  | No       | -                      | Basic auth password for the callback URL |
| `INTERVAL`                               | `int64`   | No       | `10` seconds           | How often (in seconds) should the check be made |
| `MAX_DURATION`                           | `int64`   | No       | `60` seconds           | How long (in seconds) should the container be observed |
| `AWS_ECS_TASK_METADATA_ENDPOINT`         | `string`  | No       | `http://169.254.170.2` | AWS ECS task metadata endpoint base URL |
| `AWS_ECS_TASK_METADATA_ENDPOINT_VERSION` | `string`  | No       | `v2`                   | AWS ECS task metadata endpoint API version |

## How It Works?

![Overview Details](docs/images/overview-details.png)

1. after the main container has stopped
2. the sidecar container detects this (by repetitively calling the AWS ECS task metadata endpoint)
3. and calls the callback URL

Here is a more detailed view in form of a sequence diagram:

![Sequence Diagram](docs/images/sequence-diagram.png)
